rule unzip_fastq:
    input:
        "data/{sample}.fastq.gz"
    output:
        temp("tmp/{sample}.fastq")
    shell:
        """
            mkdir -p tmp
            gunzip -c {input} > {output}
        """

rule trim:
    input:
        "tmp/{sample}.fastq"
    output:
        temp("tmp/{sample}_shorter.fastq")
    conda:
        "env.yaml"
    shell:
        "fastx_trimmer -f 1 -l 100 -Q {config[encoding_quality]} -i {input} -o {output}"

rule make_adapter_seq:
    output:
        temp("tmp/adapterSeq.fa")
    shell:
        """
            echo -e ">illumina_adapter_forward\n{config[adapter_seq]}" > tmp/adapterSeq.fa
        """

rule compute_reverse_complement:
    input:
        "tmp/adapterSeq.fa"
    output:
        temp("tmp/adapterSeqRevComp.fa")
    conda:
        "env.yaml"
    shell:
        "fastx_reverse_complement -i {input} -o {output}"

rule cut_adapters:
    input:
        adapter_reserve_complement = "tmp/adapterSeqRevComp.fa",
        r1 = "tmp/{sample}-R1_shorter.fastq",
        r2 = "tmp/{sample}-R2_shorter.fastq"
    output:
        r1 = temp("tmp/{sample}-R1_trimmed.fastq"),
        r2 = temp("tmp/{sample}-R2_trimmed.fastq")
    conda:
        "env.yaml"
    shell:
        "cutadapt -a {config[adapter_seq]} -A $(tail -n1 {input.adapter_reserve_complement}) -o {output.r1} -p {output.r2} {input.r1} {input.r2} > cutadapt.o 2> cutadapt.e"

rule remove_bad_quality_seq:
    input:
        r1 = "tmp/{sample}-R1_trimmed.fastq",
        r2 = "tmp/{sample}-R2_trimmed.fastq"
    output:
        r1 = "result/{sample}_R1.fastq",
        r2 = "result/{sample}_R2.fastq",
        r1_segments = temp("tmp/{sample}-R1_trimmed.fastq_trimmed.segments"),
        r2_segments = temp("tmp/{sample}-R2_trimmed.fastq_trimmed.segments"),
        r1_hist = temp("tmp/{sample}-R1_trimmed.fastq_trimmed.segments_hist.pdf"),
        r2_hist = temp("tmp/{sample}-R2_trimmed.fastq_trimmed.segments_hist.pdf")
    conda:
        "env.yaml"
    shell:
        """
            SolexaQA++ dynamictrim -h {config[phred_quality_score]} -d tmp {input.r1} {input.r2}
            mkdir -p result
            mv {input.r1}.trimmed {output.r1}
            mv {input.r2}.trimmed {output.r2}
        """

rule clean:
    shell:
        """
            rm -rf tmp
            rm -rf result
            rm .out
        """
